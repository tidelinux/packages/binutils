# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://sourceware.org/pub/binutils/releases/binutils-${BPM_PKG_VERSION}.tar.xz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  mkdir -v build
  cd       build
  ../configure --prefix=/usr       \
             --sysconfdir=/etc   \
             --enable-gold       \
             --enable-ld=default \
             --enable-plugins    \
             --enable-shared     \
             --disable-werror    \
             --enable-64-bit-bfd \
             --with-system-zlib  \
             --enable-default-hash-style=gnu
  make tooldir=/usr
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  cd build
  make check
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  cd build
  make DESTDIR="$BPM_OUTPUT" tooldir=/usr install
  rm -fv "$BPM_OUTPUT"/usr/lib/lib{bfd,ctf,ctf-nobfd,gprofng,opcodes,sframe}.a
  mkdir -p "$BPM_OUTPUT"/usr/share/licenses/binutils/
  cp ../COPYING "$BPM_OUTPUT"/usr/share/licenses/binutils/COPYING
}
